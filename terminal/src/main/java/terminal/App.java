package terminal;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

import utilidades.propriedades.LerProperties;
import utilidades.propriedades.SalvaProperties;

public class App {

	public static void main(String[] args) {
		System.out.println("TERMINAL APP");

		propriedades();
		serializacao();

	}

	private static void serializacao() {
		System.out.println(":::: SERIALIZACAO ::::");
		System.out.println("");
		
		String xml = "<Usuario><nome>Fulano</nome></Usuario>";
		XStream x = new XStream();
		x.alias("Usuario", Usuario.class);
		Usuario u = (Usuario) x.fromXML(xml);
		System.out.println(u.getNome());
		String strXml = x.toXML(u);
		System.out.println(strXml);
			
		String json = "{ \"nome\" : \" fulano\" }";
		Gson j = new Gson();
		Usuario u2 = j.fromJson(json, Usuario.class);
		System.out.println(u2.getNome());
		String strJson = j.toJson(u2);
		System.out.println(strJson);
		
		System.out.println("");
	}
	

	private static void propriedades() {
		System.out.println(":::: PROPRIEDADES ::::");
		System.out.println("");
		
		try {
			LerProperties prop = new LerProperties("arquivo-leitura.properties");
			System.out.println("ARQUIVO LEITURA: " + prop.getPropriedade("nome"));
			
			SalvaProperties salvProp = new SalvaProperties();
			salvProp.setPropriedade("nome", "Filipe");
			salvProp.setPropriedade("sobrenome", "Costa S.");
			salvProp.setPropriedade("email", "flp.costas@gmail.com");
			salvProp.criarArquivo("arquivo-escrita.properties");
			LerProperties propEscrita = new LerProperties("arquivo-escrita.properties");
			System.out.println("ARQUIVO ESCRITA: " + propEscrita.getPropriedade("nome") + " " + propEscrita.getPropriedade("sobrenome") + " - " + propEscrita.getPropriedade("email"));
		} catch (Exception e) {
			System.out.println(e);
		}
		
		System.out.println("");
	}
	
	public class Usuario {

		private String nome;

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}
		
		
		public Usuario() {
		
		}
	}

}


