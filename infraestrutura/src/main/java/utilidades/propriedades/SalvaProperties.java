package utilidades.propriedades;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Properties;

public class SalvaProperties {

	Properties properties = new Properties();

	public void criarArquivo(String arquivo) throws Exception {

		try {
			File file = new File(arquivo);
			FileOutputStream fos = new FileOutputStream(file);
			properties.store(fos, "Cria o arquivo");
			fos.flush();
			fos.close();
		} catch (Exception ex) {
			throw new Exception("Erro ao tentar criar o arquivo de properties", ex);
		}
	}

	public Properties setPropriedade(String chave, String valor) {
		properties.setProperty(chave, valor);
		return properties;
	}

}
