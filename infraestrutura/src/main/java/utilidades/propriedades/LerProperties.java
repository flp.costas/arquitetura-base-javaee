package utilidades.propriedades;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class LerProperties {

	private Properties properties;

	public LerProperties(String arquivo) throws Exception {
		try {

			FileInputStream fis = null;
			File file = new File(arquivo);

			if (file.exists()) {
				properties = new Properties();
				fis = new FileInputStream(file);
				properties.load(fis);
				fis.close();
			}

		} catch (Exception ex) {
			throw new Exception("Erro ao tentar abrir o arquivo de properties",
					ex);
		}
	}

	public String getPropriedade(String chave) {
		if (properties != null) {
			return properties.getProperty(chave);
		}
		return null;
	}
}
