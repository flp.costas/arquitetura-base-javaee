package utilidades.serializacao;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

public class Conversor {

	public static Object converteXmlEmObjeto(String stringXml, String aliase, Class<?> classe) {
		XStream xtream = new XStream();
		xtream.alias(aliase, classe);
		return xtream.fromXML(stringXml);
	}

	public static Object converteJsonEmObjeto(String stringJson, Class<?> classe) {
		Gson gson = new Gson();
		return gson.fromJson(stringJson, classe);
	}

	public static String converteObjetoEmXml(Object objeto) {
		XStream xtream = new XStream();
		return xtream.toXML(objeto);
	}

	public static String converteObjetoEmJson(Object objeto) {
		Gson gson = new Gson();
		return gson.toJson(objeto);
	}
}
