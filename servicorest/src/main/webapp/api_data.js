define({ "api": [
  {
    "type": "post",
    "url": "/usuario/cadastrar",
    "title": "Cadastra Usuario",
    "name": "Cadastrar",
    "group": "Usuario",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Objeto",
            "description": "<p>rest.modelo.Usuario em Json ou Xml</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"id\":1,\n      \"nome\":\"Filipe Costa\",\n      \"email\":\"flp.costas@gmail.com\"\n    }",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Objeto",
            "description": "<p>rest.modelo.Usuario em Json ou Xml</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "    {\n      \"id\":0,\n      \"nome\":\"Filipe Costa\",\n      \"email\":\"flp.costas@gmail.com\"\n    }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "c:/apidoc/servicorest/UsuarioService.java",
    "groupTitle": "Usuario"
  },
  {
    "type": "put",
    "url": "/usuario/editar",
    "title": "Editar Usuario",
    "name": "Editar",
    "group": "Usuario",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Objeto",
            "description": "<p>rest.modelo.Usuario em Json ou Xml</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"id\":3,\n      \"nome\":\"Filipe Costa\",\n      \"email\":\"flp.costas@gmail.com\"\n    }",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Objeto",
            "description": "<p>rest.modelo.Usuario em Json ou Xml</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "    {\n      \"id\":3,\n      \"nome\":\"Filipe Costa\",\n      \"email\":\"flp.costas@gmail.com\"\n    }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "c:/apidoc/servicorest/UsuarioService.java",
    "groupTitle": "Usuario"
  },
  {
    "type": "get",
    "url": "/usuario/listar",
    "title": "Listar Usuario",
    "name": "Listar",
    "group": "Usuario",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Objeto",
            "description": "<p>usuario em Json ou Xml</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    [{\n      \"id\":1,\n      \"nome\":\"Filipe Costa\",\n      \"email\":\"flp.costas@gmail.com\"\n    },\n    {\n      \"id\":2,\n      \"nome\":\"Filipe Costa\",\n      \"email\":\"flp.costas@gmail.com\"\n    }]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "c:/apidoc/servicorest/UsuarioService.java",
    "groupTitle": "Usuario"
  },
  {
    "type": "get",
    "url": "/usuario/test/{token}",
    "title": "Requisicao de teste",
    "name": "Test",
    "group": "Usuario",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Objjeto",
            "description": "<p>containter de requisicao.</p> "
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "token",
            "description": "<p>Optional Firstname of the User.</p> "
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "c:/apidoc/servicorest/UsuarioService.java",
    "groupTitle": "Usuario"
  }
] });