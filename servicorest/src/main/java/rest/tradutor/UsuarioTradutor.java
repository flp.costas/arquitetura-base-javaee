package rest.tradutor;

import java.util.ArrayList;
import java.util.List;

import dominio.fachada.UsuarioFachadaImp;
import dominio.interfaces.fachada.UsuarioFachada;

public class UsuarioTradutor {

	UsuarioFachada fachada;
	dominio.entidade.Usuario entidade;
	rest.modelo.Usuario modelo;

	public UsuarioTradutor() {
		fachada = new UsuarioFachadaImp();
		entidade = new dominio.entidade.Usuario();
		modelo = new rest.modelo.Usuario();
	}

	public List<rest.modelo.Usuario> listarTodos() {
		List<rest.modelo.Usuario> listaModelo = new ArrayList<>();
		List<dominio.entidade.Usuario> listaEntidade = new ArrayList<>();
		listaEntidade = fachada.lista();

		for (dominio.entidade.Usuario lista : listaEntidade) {
			rest.modelo.Usuario u = new rest.modelo.Usuario();
			u.setId(lista.getId());
			u.setEmail(lista.getEmail());
			u.setNome(lista.getNome());
			listaModelo.add(u);
		}

		return listaModelo;
	}

	public rest.modelo.Usuario salvar(rest.modelo.Usuario modelo) {
		entidade.setId(modelo.getId());
		entidade.setEmail(modelo.getEmail());
		entidade.setNome(modelo.getNome());
		fachada.setUsuario(entidade);
		fachada.salva();
		return modelo;
	}
	
	
	public rest.modelo.Usuario consultarPorId(int id) {
		entidade = fachada.consultaPorId(id);
		modelo.setId(entidade.getId());
		modelo.setNome(entidade.getNome());
		modelo.setEmail(entidade.getEmail());
		return modelo;
	}

}
