package rest.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.JAXBElement;

import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.ContainerResponse;

import rest.modelo.GenericaList;
import rest.modelo.MensagemErro;
import rest.modelo.Usuario;
import rest.tradutor.UsuarioTradutor;
import utilidades.serializacao.Conversor;

@Path("/service/usuario")
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public class UsuarioService {

	private MensagemErro erro = new MensagemErro();
	private UsuarioTradutor tradutor;
	@Context
	private ContainerRequest containerRequest;
	@Context
	private ContainerResponse containerResponse;

	public UsuarioService() {
		tradutor = new UsuarioTradutor();
	}

	/**
	 * @api {get} /usuario/test/{token} Requisicao de teste
	 * @apiName Test
	 * @apiGroup Usuario
	 *
	 * @apiSuccess {String} Objjeto containter de requisicao.
	 * @apiParam {string} [token] Optional Firstname of the User.
	 */
	@GET
	@Path("/test/{token}")
	public String test(@PathParam("token") String token) {
		try {
			return "test" + containerRequest.toString();
		} catch (WebApplicationException ex) {
			erro.setException(ex.getMessage() + " "
					+ ex.getStackTrace().toString());
			erro.setMensagem("Mensagem capturada");
			throw new WebApplicationException(Response.status(400)
					.entity(Conversor.converteObjetoEmJson(erro)).build());
		}
	}

	/**
	 * @api {get} /usuario/listar Listar Usuario
	 * @apiName Listar
	 * @apiGroup Usuario
	 *
	 * @apiSuccess {string} Objeto usuario em Json ou Xml
	 *
	 * @apiSuccessExample Success-Response: HTTP/1.1 200 OK [{ "id":1,
	 *                    "nome":"Filipe Costa", "email":"flp.costas@gmail.com"
	 *                    }, { "id":2, "nome":"Filipe Costa",
	 *                    "email":"flp.costas@gmail.com" }]
	 *
	 */
	@GET
	@Path("/listar")
	public Response listar() {
		try {
			List<rest.modelo.Usuario> l = tradutor.listarTodos();
			return Response.status(Status.OK).entity(new GenericEntity<List<rest.modelo.Usuario>>(l){}).build();

		} catch (Exception ex) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
	}

	/**
	 * @api {post} /usuario/cadastrar Cadastra Usuario
	 * @apiName Cadastrar
	 * @apiGroup Usuario
	 *
	 * @apiSuccess {string} Objeto rest.modelo.Usuario em Json ou Xml
	 * @apiParam {string} Objeto rest.modelo.Usuario em Json ou Xml
	 * 
	 * @apiSuccessExample Success-Response: HTTP/1.1 200 OK { "id":1,
	 *                    "nome":"Filipe Costa", "email":"flp.costas@gmail.com"
	 *                    }
	 * 
	 * @apiParamExample {json} Request-Example: { "id":0, "nome":"Filipe Costa",
	 *                  "email":"flp.costas@gmail.com" }
	 *
	 */
	@POST
	@Path("/cadastrar")
	public Response cadastrar(String usuario) {
		try {
			String tipo = containerRequest.getHeaderString("Content-Type");
			rest.modelo.Usuario u = tipo.equals("application/xml") ? (Usuario) Conversor
					.converteXmlEmObjeto(usuario, "rest.modelo.Usuario",
							rest.modelo.Usuario.class) : (Usuario) Conversor
					.converteJsonEmObjeto(usuario, rest.modelo.Usuario.class);

			tradutor.salvar(u);

			Object o = new Object();
			if (tipo.equals("application/xml"))
				o = Conversor.converteObjetoEmXml(u);
			else
				o = Conversor.converteObjetoEmJson(u);

			return Response.status(201).entity(o).build();

		} catch (Exception e) {
			throw new WebApplicationException("erro");
		}
	}

	/**
	 * @api {put} /usuario/editar Editar Usuario
	 * @apiName Editar
	 * @apiGroup Usuario
	 *
	 * @apiSuccess {string} Objeto rest.modelo.Usuario em Json ou Xml
	 * @apiParam {string} Objeto rest.modelo.Usuario em Json ou Xml
	 * 
	 * @apiSuccessExample Success-Response: HTTP/1.1 200 OK { "id":3,
	 *                    "nome":"Filipe Costa", "email":"flp.costas@gmail.com"
	 *                    }
	 * 
	 * @apiParamExample {json} Request-Example: { "id":3, "nome":"Filipe Costa",
	 *                  "email":"flp.costas@gmail.com" }
	 *
	 */
	@PUT
	@Path("/editar")
	public Response editar(String usuario) {
		try {
			String tipo = containerRequest.getHeaderString("Content-Type");
			rest.modelo.Usuario u = tipo.equals("application/xml") ? (Usuario) Conversor
					.converteXmlEmObjeto(usuario, "rest.modelo.Usuario",
							rest.modelo.Usuario.class) : (Usuario) Conversor
					.converteJsonEmObjeto(usuario, rest.modelo.Usuario.class);

			rest.modelo.Usuario usuarioEncontrado = tradutor.consultarPorId(u
					.getId());

			if (usuarioEncontrado != null)
				tradutor.salvar(u);

			Object o = new Object();
			if (tipo.equals("application/xml"))
				o = Conversor.converteObjetoEmXml(u);
			else
				o = Conversor.converteObjetoEmJson(u);

			return Response.status(203).entity(o).build();

		} catch (Exception e) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
	}

	@GET
	@Path("/xml")
	public Response xml() {
		List<rest.modelo.Usuario> l = tradutor.listarTodos();
		return Response.ok(Conversor.converteObjetoEmXml(l)).build();
	}

	@GET
	@Path("/json")
	public Response json() {
		List<rest.modelo.Usuario> l = tradutor.listarTodos();
		return Response.ok(Conversor.converteObjetoEmJson(l)).build();
	}

	@GET
	@Path("/xmljson")
	public Response xmjson() {
		Usuario u = new Usuario();
		u.setId(1);
		u.setNome("Fiipe");
		u.setEmail("f@mail.com");
		return Response.status(Status.OK).entity(u).build();
	}
	
}
