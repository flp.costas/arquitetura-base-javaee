package rest.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/service/erros")
@Produces({ MediaType.TEXT_PLAIN, MediaType.TEXT_HTML,
		MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Consumes({ MediaType.TEXT_PLAIN, MediaType.TEXT_HTML,
		MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public class ErrosService {

	@GET
	@Path("/badrequest")
	public Response badRequest() {
		return Response.status(Status.BAD_REQUEST).entity("{ \"mensagem\" : \"bad resquest\" }")
				.build();
	}

	@GET
	@Path("/notfound")
	public Response erro() {
		return Response.status(Status.NOT_FOUND).entity("not found").build();
	}

	@GET
	@Path("/webappexception")
	public void webAppException() {
		throw new WebApplicationException(Status.BAD_REQUEST);
	}

}
