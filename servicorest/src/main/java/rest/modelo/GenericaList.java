package rest.modelo;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="lista")
public class GenericaList<T> extends ArrayList<T> {

	private static final long serialVersionUID = 1L;

}
