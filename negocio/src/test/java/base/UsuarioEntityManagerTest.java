package base;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dominio.entidade.Usuario;

/**
 * Executar testes separado para essa classe
 * 
 * @author Filipe
 *
 */
public class UsuarioEntityManagerTest {

	private static final String UNIDADE_PERSISTENCIA = "default_mysql";
	private EntityManagerFactory factory;
	private EntityManager manager;

	@Before
	public void inicioTest() {
		factory = Persistence.createEntityManagerFactory(UNIDADE_PERSISTENCIA);
		manager = factory.createEntityManager();
	}

	@Test
	public void UsuarioCrudTest() {
		System.out.println("Inserir usuario");
		criaUsuario();

		System.out.println("Excluir usuario");
		excluiUsuario();
	}

	@After
	public void fimTest() {
		manager.close();
		factory.close();
	}

	private void criaUsuario() {

		Usuario u1 = new Usuario();
		u1.setEmail("flp.costas@gmail.com");
		u1.setNome("Filipe");
		Usuario u2 = new Usuario();
		u2.setEmail("julianascosta@gmail.com");
		u2.setNome("Juliana");
		Usuario u3 = new Usuario();
		u3.setEmail("cesouza@gmail.com");
		u3.setNome("Carolina");

		EntityTransaction transaction = manager.getTransaction();
		transaction.begin();
		manager.persist(u1);
		manager.persist(u2);
		manager.persist(u3);
		transaction.commit();

		assertNotNull("item criado com sucesso", u1);
		assertNotNull("item criado com sucesso", u2);
		assertNotNull("item criado com sucesso", u3);
	}

	private void excluiUsuario() {
		Usuario u1 = manager.getReference(Usuario.class, 2);
		EntityTransaction transaction = manager.getTransaction();
		transaction.begin();
		manager.remove(u1);
		transaction.commit();

		TypedQuery<Usuario> query = manager.createQuery(
				"select u from Usuario u where u.id = :id", Usuario.class);
		query.setParameter("id", 2);
		int excluido = query.getFirstResult();

		assertEquals("item excluido com sucesso", 0, excluido);
	}
	
	
	private void editaUsuario() {
		
	}
	
	private void excluirUsuario() {
	
	}
}
