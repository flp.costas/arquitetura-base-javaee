package base;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import dominio.entidade.Usuario;
import dominio.fachada.UsuarioFachadaImp;
import dominio.interfaces.fachada.UsuarioFachada;

public class UsuarioAppTest {

	private UsuarioFachada fachada;

	@Before
	public void inicioTest() {
		// instancia que precisa ser injetada
		fachada = new UsuarioFachadaImp();
	}

	@After
	public void fimTest() {

	}

	@Test
	public void test1() {
		cadastro();
		listar();
		excluir();
		editar();
	}

	private void cadastro() {
		Usuario u1 = new Usuario();
		u1.setEmail("flp.costas@gmail.com");
		u1.setNome("Filipe");
		Usuario u2 = new Usuario();
		u2.setEmail("juliana@gmail.com");
		u2.setNome("Juliana");
		Usuario u3 = new Usuario();
		u3.setEmail("caroline.souza@gmail.com");
		u3.setNome("Caroline");
		fachada.setUsuario(u1);
		fachada.salva();
		fachada.setUsuario(u2);
		fachada.salva();
		fachada.setUsuario(u3);
		fachada.salva();

		assertNotNull("item 1 inserido com sucesso", u1);
		assertNotNull("item 2 inserido com sucesso", u2);
		assertNotNull("item 3 inserido com sucesso", u3);
	}

	private void listar() {
		List<Usuario> lista = fachada.lista();
		for (Usuario u : lista) {
			System.out.println("email: " + u.getEmail());
		}

		assertEquals("itens listados com sucesso", lista.size(), 3);
	}

	private void editar() {
		Usuario u1 = new Usuario();
		u1.setId(2);
		u1.setEmail("julianascosta@gmail.com");
		u1.setNome("JULIANA COSTA");
		fachada.setUsuario(u1);
		fachada.salva();
		Usuario usuarioEditado = fachada.consultaPorId(2);

		assertEquals("editado com sucesso", usuarioEditado.getEmail(),
				"julianascosta@gmail.com");
	}

	private void excluir() {
		fachada.exclui(1);
		Usuario usuarioExcluido = fachada.consultaPorId(1);
		org.junit.Assert.assertNull("excluido com sucesso", usuarioExcluido);
	}

}
