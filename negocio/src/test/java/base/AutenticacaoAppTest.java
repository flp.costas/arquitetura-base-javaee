package base;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import dominio.entidade.Usuario;
import dominio.fachada.UsuarioFachadaImp;
import dominio.interfaces.fachada.UsuarioFachada;
import dominio.interfaces.servico.AutenticacaoServico;
import dominio.servico.AutenticacaoServicoImp;

public class AutenticacaoAppTest {

	private UsuarioFachada fachada;
	private AutenticacaoServico servico;
	private Usuario usuario;

	@Before
	public void inicioTest() {
		
		// instancia que precisa ser injetada
		servico = new AutenticacaoServicoImp();
		fachada = new UsuarioFachadaImp();

		usuario = new Usuario();
		usuario.setEmail("julianascosta@gmail.com");
		usuario.setNome("Juliana");
		fachada.setUsuario(usuario);
		fachada.salva();

	}

	@After
	public void fimTest() {

	}

	@Test
	public void test1() {
		Usuario u = new Usuario();
		u.setId(1);
		u.setEmail("julianascosta@gmail.com");
		Assert.assertTrue("autenticado com sucesso", servico.login(u));
	}

}
