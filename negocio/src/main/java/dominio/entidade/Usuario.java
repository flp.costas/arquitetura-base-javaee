package dominio.entidade;

import javax.persistence.*;
import javax.xml.bind.annotation.*;

import dominio.interfaces.anotacoes.*;

@Entity
@Table(name="Usuario")
public class Usuario {

	@Id
	@GeneratedValue
	private Integer id;
	@ValidarPresenca(mensagem = "Nome obrigatorio")
	private String nome;
	@ValidarPresenca(mensagem = "Email obrigatorio")
	private String email;

	public Usuario() {
		// ValidarPresencaImp.executar(ValidarPresenca.class, this);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
