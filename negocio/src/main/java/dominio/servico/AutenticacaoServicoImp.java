package dominio.servico;

import dominio.entidade.Usuario;
import dominio.fachada.UsuarioFachadaImp;
import dominio.interfaces.fachada.UsuarioFachada;
import dominio.interfaces.servico.AutenticacaoServico;

public class AutenticacaoServicoImp implements AutenticacaoServico {

	UsuarioFachada usuarioFachada;
	Usuario login;

	public AutenticacaoServicoImp() {
		usuarioFachada = new UsuarioFachadaImp();
	}

	public boolean login(Usuario usuario) {
		login = usuarioFachada.consultaPorId(usuario.getId());
		if (login.getEmail() == usuario.getEmail())
			return true;
		else
			return false;
	}

	public Usuario getLogin() {
		if (login != null)
			return login;
		else
			return null;
	}
}
