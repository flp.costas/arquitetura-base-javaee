package dominio.interfaces.fachada;

import java.util.List;

import dominio.entidade.Usuario;

public interface UsuarioFachada {

	public void setUsuario(Usuario u);

	public Usuario getUsuario();

	public void salva();

	public List<Usuario> lista();

	public void exclui(int id);
	
	public Usuario consultaPorId(int id);

}
