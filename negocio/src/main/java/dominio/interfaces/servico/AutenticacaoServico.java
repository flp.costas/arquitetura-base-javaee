package dominio.interfaces.servico;

import dominio.entidade.Usuario;

public interface AutenticacaoServico {

	public boolean login(Usuario usuario);

	public Usuario getLogin();

}
