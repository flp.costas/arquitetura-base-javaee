package dominio.fachada;

import java.util.List;

import javax.persistence.EntityManager;

import dominio.entidade.Usuario;
import dominio.interfaces.fachada.UsuarioFachada;
import persistencia.dao.JpaUtil;
import persistencia.dao.UsuarioDaoImp;
import persistencia.interfaces.dao.UsuarioDao;

public class UsuarioFachadaImp implements UsuarioFachada {

	private EntityManager entityManager;
	private UsuarioDao dao;
	private Usuario usuario = new Usuario();

	public UsuarioFachadaImp() {
		// instancias que precisam serem injetadas
		entityManager = JpaUtil.getEntityManager("default_mysql");
		dao = new UsuarioDaoImp(entityManager);
	}

	public void setUsuario(Usuario u) {
		this.usuario = u;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void salva() {
		usuario = dao.salvar(usuario);
	}

	public List<Usuario> lista() {
		return dao.listar();
	}

	public void exclui(int id) {
		dao.excluir(id);
	}

	public Usuario consultaPorId(int id) {
		Usuario usuario = dao.consultarPorId(id);
		return usuario;
	}

	public void fecharJpa() {
		JpaUtil.fechaManager(entityManager);
		JpaUtil.fechaManagerFactory();
	}

}
