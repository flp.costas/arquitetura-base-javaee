package dominio.anotacoes;

import java.lang.reflect.Field;

import dominio.excecoes.ValidarPresencaExcecao;
import dominio.interfaces.anotacoes.ValidarPresenca;

/**
 * 
 * @author Filipe
 *
 * Classe responsavel por criar a implementacao de 
 * validar a presenca do valor do campo.
 * O Annotation é passado por um parametro ao metodo que executa e testado na condicao de loop
 * para percorrer os atributos necessarios.
 *
 */
public class ValidarPresencaImp {
	
	public static void executar(Class<ValidarPresenca> annotation, Object o) {
		Class<?> classe = o.getClass();
		for (Field f : classe.getDeclaredFields()) {
            f.setAccessible(true);
            if (f.isAnnotationPresent(annotation)) {
                try {
                	ValidarPresenca ann = f.getAnnotation(annotation);
                	if(f.get(o).toString().isEmpty()) {
                		if(ann.mensagem().isEmpty())
                			throw new ValidarPresencaExcecao("Default: Preencha o campo: " + f.getName());                			
                		else
                			throw new ValidarPresencaExcecao("Custom: "+ ann.mensagem());
                	}
                	                	
				} catch (IllegalArgumentException e) {
					System.out.println(e.getMessage());
				} catch (IllegalAccessException e) {
					System.out.println(e.getMessage());
				}
            }
        }
		
	}
}
