package persistencia.interfaces.dao;

import java.util.List;

import dominio.entidade.Usuario;

public interface UsuarioDao {

	public Usuario salvar(Usuario u);
	public List<Usuario> listar();
	public void excluir(int id);
	public Usuario consultarPorId(int id);
}
