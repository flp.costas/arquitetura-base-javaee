package persistencia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import persistencia.interfaces.dao.UsuarioDao;
import dominio.entidade.Usuario;

public class UsuarioDaoImp implements UsuarioDao {

	private EntityManager entityManager;

	public UsuarioDaoImp(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Usuario salvar(Usuario u) {

		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		if (u.getId() == null) {
			entityManager.persist(u);
		} else {
			u = entityManager.merge(u);
		}
		transaction.commit();

		return u;
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> listar() {
		Query query = null;
		query = entityManager.createQuery("select u from Usuario u", Usuario.class);

		return query.getResultList();
	}

	public void excluir(int id) {
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		Usuario u = entityManager.getReference(Usuario.class, id);
		entityManager.remove(u);
		transaction.commit();
	}

	public Usuario consultarPorId(int id) {
		Usuario query = null;
		query = entityManager.find(Usuario.class, id);
		return query;
	}

}
