package persistencia.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Classe responsavel por fabricar EntityManager da JPA
 * 
 * @author Filipe
 *
 */
public class JpaUtil {

	private static EntityManagerFactory factory;

	public static EntityManager getEntityManager() {
		return getEntityManager("default");
	}
	
	public static EntityManager getEntityManager(String persistenceUnity) {
		factory = Persistence.createEntityManagerFactory(persistenceUnity);
		return factory.createEntityManager();
	}

	public static void fechaManagerFactory() {
		factory.close();
	}

	public static void fechaManager(EntityManager manager) {
		manager.close();
	}

}
